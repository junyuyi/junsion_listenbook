<?php
global $_W,$_GPC;
$op = $_GPC['op'] ? $_GPC['op'] : 'display';
$weid = $_W['uniacid'];
if($op == 'display'){
	$condition = '';
	$pindex = max(1,$_GPC['page']);
	$psize = 20;
	
	$nickname = $_GPC['nickname'];
	if(!empty($nickname)){
		$mems = getall('select id,nickname,avatar from '.tb('mem')." where uniacid='{$weid}' and nickname like '%{$nickname}%'",'id');
		if (!empty($mems)){
			$mids = array_keys($mems);
			$condition .= " and mid in (".implode(',', $mids).")"; 
		}else{
			$condition .= " and mid =0";
		}
	}
	$ordersn = $_GPC['ordersn'];
	if(!empty($ordersn)){
		$condition .= " and ordersn like '%{$ordersn}%'";
	}
	if($_GPC['status']!= ''){
		$condition .= " and status = '{$_GPC['status']}'";
		$con = " and status = '{$_GPC['status']}'";
	}
	$bid = intval($_GPC['bid']);
	if ($bid){
		$condition .= " and bid='{$bid}'";
		$con .= " and bid='{$bid}'";
	}
	$list = getall('select * from '.tb('order')." where uniacid='{$weid}' {$condition} order by id desc limit ".($pindex-1)*$psize.','.$psize);
	$total = col('select count(1) from ' . tb('order') . " where uniacid='{$weid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
	if (!empty($list)){
		foreach ($list as $l){
			$mids[] = $l['mid'];
			$bids[] = $l['bid'];
			$oids[] = $l['id'];
		}
		if (empty($mems)){
			$mems = getall('select id,nickname,avatar from '.tb('mem')." where uniacid='{$weid}' and id in (".implode(',', $mids).")",'id');
		}
		$mbs = getall('select oid,uname,mobile from '.tb('mem_book')." where oid in (".implode(',', $oids).")",'oid');
	}
	$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}'",'id');
	$orders = getall("select createtime,price from ".tb('order')." where uniacid='{$_W['uniacid']}' {$con}");
	foreach ($orders as $value) {
		if (date('Ymd',$value['createtime']) == date('Ymd')){
			$today++;
			$today2 += $value['price'];
		}else{
			$t = mktime(0,0,0,date('m'),date('d')-1,date('Y'));
			if (date('Ymd',$value['createtime']) == date('Ymd',$t)){
				$yes++;
				$yes2 += $value['price'];
			}
		}
		$all++;
		$all2 += $value['price'];
	}
	
}

if($op == 'check'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('order') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('order'),'error');
	}
	if(update('order',['status'=>5],['id'=>$id]) === false) MSG('操作失败',referer(),'error');
	else MSG('操作成功',$this->wurl('order'),'success');
}
if($op == 'refund'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('order') . " where id = '{$id}'");
	if(empty($item) || $item['status']!=5){
		MSG('该记录不存在或已被删除',$this->wurl('order'),'error');
	}
	$res = $this->wechatRefund($item['transid'], $item['price'], $item['price']);
	if($res['errcode']==0){
		update('order',['status'=>6],['id'=>$id]);
		update('mem_book',['status'=>1],['bid'=>$item['bid'],'mid'=>$item['mid']]);
		MSG('操作成功',$this->wurl('order'),'success');
	}
	else{
		MSG($res['errmsg'],referer(),'error');
	}
}
if($op == 'back'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('order') . " where id = '{$id}'");
	if(empty($item) || $item['status']!=7){
		MSG('该记录不存在或已被删除',$this->wurl('order'),'error');
	}
	if ($item['paytype'] == 2){
		load()->model('mc');
		$mem = get("select openid from ".tb('mem')." where id='{$item['mid']}'");
		$fans = mc_fetch($mem['openid'],array('uid'));
		mc_credit_update($fans['uid'],'credit2',$item['price']);
		$res['errcode'] = 0;
	}else{
		$res = $this->wechatRefund($item['transid'], $item['price'], $item['price']);
	}
	if($res['errcode']==0){
		update('order',['status'=>8],['id'=>$id]);
		MSG('操作成功',$this->wurl('order'),'success');
	}
	else{
		MSG($res['errmsg'],referer(),'error');
	}
}
if($op == 'del'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('order') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('order'),'error');
	}
	if(del('order',array('id'=>$id)) === false) MSG('操作失败',referer(),'error');
	else MSG('操作成功',$this->wurl('order'),'success');
}
include $this->template('order');