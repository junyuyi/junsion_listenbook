<?php
global $_W, $_GPC;
load()->func('file');
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'upload';
if ($operation == 'upload') {
	$field = $_GPC['file'];
	if (!empty($_FILES[$field]['name'])) {
		if ($_FILES[$field]['error'] != 0) {
			$result['message'] = '上传失败，请重试！';
			exit(json_encode($result));
		}
		$path = IA_ROOT.'/addons/junsion_listenbook/qrcode/';
		if (!file_exists($path)){
			mkdir($path,0777);
		}
		$filename = $path.'qr_'.md5(time().rand(10000,99999)).'.jpg';
		$res = move_uploaded_file($_FILES[$field]["tmp_name"],$filename);
		if (!$res){
			$result['message'] = '上传失败，请重试！';
			die(json_encode($result));
		}
		$filename = toimage(str_replace(IA_ROOT.'/', $_W['siteroot'], $filename));
		$cfg = $this->module['config'];
		if ($cfg['qiniu']['isqiniu']==1){//七牛存储
			$url = $filename;
			$filename = $this->uploadQiniu($url,$cfg['qiniu'],'.jpg');
			unlink($url);
		}
		$result['status']   = "success";
		$result['filename'] = $filename;
		$result['url'] = $result['filename'];
		die(json_encode($result));
	} else {
		$result['message'] = '请选择要上传的图片！';
		die(json_encode($result));
	}
}