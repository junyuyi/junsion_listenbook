<?php
global $_W,$_GPC;
$title = '报名';
$weid = $_W['uniacid'];
$mem = $this->getMem();
$id = $_GPC['id'];
$book = get('select * from '.tb('book')." where id='{$id}' and uniacid='{$weid}'");
if (empty($book)){
	MSG('课堂不存在');
}
$old = get('select * from '.tb('order')." where mid='{$mem['id']}' and uniacid='{$weid}' order by createtime desc");
$cfg = $this->module['config'];
if ($_W['ispost']){
	$uname = $_GPC['uname'];
	$mobile = $_GPC['mobile'];
	$paytype = $_GPC['paytype'];
	$data = array(
		'uniacid' => $weid,
		'bid' => $id,
		'mid' => $mem['id'],
		'ordersn' => date('YmdHis').random(3,1),
		'uname' => $uname,
		'mobile' => $mobile,
		'paytype' => $paytype,
		'price' => $book['price'],
		'createtime' => time(),
	);
	insert('order',$data);
	$oid = pdo_insertid();
	header('location:'.$this->murl('pay',array('id'=>$oid)));
	exit;
}
if ($cfg['creditswitch']==1){
	load()->model('mc');
	$fans = mc_fetch($mem['openid'],array('credit2','uid'));
}
$share = $this->getShare($mem, $cfg);
include $this->template('buy');