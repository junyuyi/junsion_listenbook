<?php
global $_W,$_GPC;
$op = $_GPC['op'] ? $_GPC['op'] : 'display';
$weid = $_W['uniacid'];

if(empty($mem_book['guide_weid'])) $mem_book['guide_weid'] = $_W['uniacid'];
$qrUrl = $_W['siteroot'].'attachment/qrcode_'.$mem_book['guide_weid'].'.jpg?time='.time();


if($op == 'display'){
	$condition = '';
	$pindex = max(1,$_GPC['page']);
	$psize = 20;
	
	$title = $_GPC['title'];
	if(!empty($title)){
		$condition .= " and title like '%{$title}%'";
	}
	$list = getall('select * from '.tb('book')." where uniacid='{$weid}' {$condition} order by sort desc, id desc limit ".($pindex-1)*$psize.','.$psize);
	$total = col('select count(1) from ' . tb('book') . " where uniacid='{$weid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}

if($op == 'post'){
	$id = intval($_GPC['id']);
	$wechats = pdo_fetchall('select uniacid,name from ' . tablename('account_wechats') . " where level = 4");
	if ($id){
		$item = get('select * from '.tb('book')." where id='{$id}' and uniacid='{$weid}'");
		$wids = explode(",", $item['weids']);
	}
	
	if (checksubmit()){
		$_GPC['poster']['data'] = json_decode(htmlspecialchars_decode($_GPC['poster']['data']),true);
		$data = array(
			'title' => trim($_GPC['title']),
			'des' => trim($_GPC['des']),
			'thumb' => trim($_GPC['thumb']),
			'author' => trim($_GPC['author']),
			'price' => trim($_GPC['price']),
			'original' => trim($_GPC['original']),
			'weids' => empty($_GPC['weids'])?'':implode(",", $_GPC['weids']),
			'isshow' => trim($_GPC['isshow']),
			'label' => trim($_GPC['label']),
			'sort' => trim($_GPC['sort']),
			'ltcolor' => trim($_GPC['ltcolor']),
			'btncolor' => trim($_GPC['btncolor']),
			'tabthumb' => trim($_GPC['tabthumb']),
			'class_num' => trim($_GPC['class_num']),
			'clock_num' => trim($_GPC['clock_num']),
			'cover' => trim($_GPC['cover']),
			'booktips' => trim($_GPC['booktips']),
			'introduce' => htmlspecialchars_decode($_GPC['introduce']),
			'selection' => htmlspecialchars_decode($_GPC['selection']),
			'paydes' => htmlspecialchars_decode($_GPC['paydes']),
			'poster' => serialize($_GPC['poster']),
			'commission' => serialize($_GPC['commission']),
		);
		if ($item){
			update('book',$data,array('id'=>$id));
		}else{
			$data['uniacid'] = $weid;
			$data['createtime'] = time();
			$data['start_day'] = $_GPC['start_day'];
			insert('book', $data);
		}
		MSG('操作成功',$this->wurl('book',array('op'=>'display')),'success');
	}
	if ($item['poster']) $poster = unserialize($item['poster']);
	if ($item['commission']) $item['commission'] = unserialize($item['commission']);
	
	$cfg = $this->module['config']['commission'];
}
		
if($op == 'del'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('book') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('book'),'error');
	}
	if(del('book',array('id'=>$id)) === false) MSG('操作失败',referer(),'error');
	else MSG('操作成功',$this->wurl('book'),'success');
}
include $this->template('book');
