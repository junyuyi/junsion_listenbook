<?php
global $_W,$_GPC;
$title = '分销说明';
$mem = $this->getMem();

$cfg = $this->module['config'];
$cfg['commission']['intro'] = htmlspecialchars_decode($cfg['commission']['intro']);

$mbid = $_COOKIE['mem_book'];
$mbook = get('select bid from '.tb('mem_book')." where mid='{$mem['id']}' and id='{$mbid}'");
$book = get('select poster from '.tb('book')." where id='{$mbook['bid']}'");
if ($book['poster']){
	$poster2 = unserialize($book['poster']);
	if ($poster2['switch']) $poster = $poster2;
}else $poster = unserialize($cfg['poster']);

$poster['bg'] = getImgBase64(toimage($poster['bg']));
$share = $this->getShare($mem, $cfg);
$mem['avatar'] = $this->saveImage($mem['avatar'],'avatar_'.$mem['id']);
$mem['avatar'] = str_replace(IA_ROOT.'/', $_W['siteroot'], $mem['avatar'].'?v='.TIMESTAMP);
$url = $_W['siteroot']."app".substr($this->createMobileUrl('index',['shareid'=>$mem['id']]),1);
$qrurl = $this->createLinkQr($mem['id'],$url);
$qrurl = str_replace('../', $_W['siteroot'], $qrurl.'?v='.TIMESTAMP);
include $this->template('commission');