<?php
global $_W,$_GPC;
$cfg = $this->module['config'];
if ($_GPC['op'] == 'send'){
	$wid = intval($_GPC['id']);
	$with = get("select * from ".tb('with')." where id='{$wid}'");
	if ($with['status']){
		MSG('该订单已提现！');
	}
	if($cfg['commission']['wtype']==0){
		$params = array('price'=>$with['price']);
		$openid = col("select openid  from ".tb('mem')." where id='{$with['mid']}'");
		$params['openid'] = $openid;
		$res = $this->sendRedPacket($params);
		if ($res['code'] == 1){
			update('with',array('status'=>1,'transid'=>$res['msg']),array('id'=>$wid));
			MSG('发放成功！',referer());
		}else{
			update('with',array('status'=>0,'transid'=>$res['msg']),array('id'=>$wid));
			MSG('发放失败！原因：'.$res['msg']);
		}
	}
	else{
		update('with',array('status'=>1,'transid'=>'手动打款'),array('id'=>$wid));
		MSG('发放成功！',referer());
	}
}elseif ($_GPC['op'] == 'sendall'){
	$withall = getall("select id,price,mid,wrate from ".tb('with')." where uniacid='{$_W['uniacid']}' and status=0 ");
	set_time_limit(0);
	foreach ($withall as $with) {
		if($cfg['commission']['wtype']==0){
			$params = array('price'=>$with['price']);
			$openid = col("select openid from ".tb('mem')." where id='{$with['mid']}'");
			$params['openid'] = $openid;
			$res = $this->sendRedPacket($params);
			if ($res['code'] == 1){
				update('with',array('status'=>1,'transid'=>$res['msg']),array('id'=>$with['id']));
			}else{
				update('with',array('status'=>0,'transid'=>$res['msg']),array('id'=>$with['id']));
			}
		}
		else{
			update('with',array('status'=>1,'transid'=>'手动打款'),array('id'=>$with['id']));
		}
	}
	MSG('发放完成！',$this->createWebUrl('with'));
}elseif ($_GPC['op'] == 'del') {
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('with') . " where id = '{$id}'");
	if(empty($item)){
		message('操作失败',$this->createWebUrl('with'),'error');
	}
	if(del('with',array('id'=>$id)) === false) message('操作失败',referer(),'error');
	else message('操作成功',$this->createWebUrl('with'),'success');
}
elseif ($_GPC['op'] == 'back') {
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('with') . " where id = '{$id}'");
	if(empty($item)){
		message('操作失败',$this->createWebUrl('with'),'error');
	}
	update('with',array('status'=>-1,'transid'=>'撤销提现'),array('id'=>$id));
	updateMemCredit(array('mid'=>$item['mid'], 'credit'=> $item['price'] + $item['wrate'], 'remark'=>'撤销提现'));
	message('操作成功',$this->createWebUrl('with'),'success');
}
$pindex = max(intval($_GPC['page']), 1);
$psize = 20;
$con = '';
if(!empty($_GPC['keyword'])){
	$con .= " and ( mid in (select id from " . tb('mem') . " where nickname like '%{$_GPC['keyword']}%' and uniacid = '{$_W['uniacid']}') )";
}
$status = intval($_GPC['status']);

$start = $_GPC['dateline']?strtotime($_GPC['dateline']['start']):(time()-30*24*3600);
$end = $_GPC['dateline']?strtotime($_GPC['dateline']['end']):time();
if(!empty($_GPC['dateline'])){
	$con .= " and createtime >= '{$start}' and  createtime <= '{$end}'";
}
$limit = " limit " . (($pindex -1) * $psize).','. $psize;
if ($_GPC['export']) $limit = '';
$list = getall('select * from ' . tb('with') . " where uniacid='{$_W['uniacid']}' and status={$status} {$con} order by createtime desc {$limit} ");
if(!empty($list)){
	foreach ($list as $value) {
		$mids[] = $value['mid'];
	}
	$mids = array_unique($mids);
	$mems = getall('select id,nickname,avatar,qr from ' . tb('mem') . " where id in (".implode(',', $mids).")",'id');
	foreach ($list as &$v){
		$v['nickname'] = $mems[$v['mid']]['nickname'];
		$v['avatar'] = $mems[$v['mid']]['avatar'];
		$v['qr'] = $mems[$v['mid']]['qr'];
	}
	unset($v);
	if ($_GPC['export']){
		set_time_limit(0);
		include_once IA_ROOT.'/addons/junsion_listenbook/excel.php';
		$filename = '提现信息_' . date('YmdHis') . '.csv';
		$exceler = new Jason_Excel_Export();
		$exceler->charset('UTF-8');
		// 生成excel格式 这里根据后缀名不同而生成不同的格式。jason_excel.csv
		$exceler->setFileName($filename);
		// 设置excel标题行
		$excel_title = array();
		$excel_title[] = '会员';
		$excel_title[] = '金额';
		$excel_title[] = '手续费';
		$excel_title[] = '流水id';
		$excel_title[] = '状态';
		$excel_title[] = '时间';
		$exceler->setTitle($excel_title);
		// 设置excel内容
		$allsum = 0;
		$all = 0;
		$uns = 0;
		foreach ($list as $val) {
			$status = "";
			if($val['status'] == 1){
				$status = "已发放";
			}else{
				$status = "待审核";
			}
			$data = array();
			$data[] = $val['nickname'];
			$data[] = $val['price'];
			$data[] = $val['wrate'];
			$data[] = $val['transid'];
			$data[] = $status;
			$data[] = date('Y-m-d H:i',$val['createtime']);
			$excel_data[] = $data;
			$allsum++;
		}
		$excel_data[] = array('总记录数:', $allsum);
		$exceler->setContent($excel_data);
		// 生成excel
		$exceler->export();
		exit;
	}
}
$total = col('select count(1) from ' . tb('with') . " where uniacid='{$_W['uniacid']}' and status={$status} {$con}");
$pager = pagination($total, $pindex, $psize);

$today = getall('select status,price from '.tb("with")." where uniacid='{$_W['uniacid']}' and status>=0 and to_days(from_unixtime(createtime)) = to_days(now()) {$con}");
$all = getall('select status,price,wrate from '.tb("with")." where uniacid='{$_W['uniacid']}' and status>=0 {$con}");
$today_num = 0;$today_money = 0;$all_num = 0;$all_money = 0;
foreach ($today as $val) {
	if ($val['status']) $today_money += $val['price'];
	else $today_num += $val['price'];
}
foreach ($all as $val) {
	if ($val['status']) $all_money += $val['price'];
	else $all_num += $val['price'];
	$all_rate += $val['wrate'];
}
include $this->template('with');