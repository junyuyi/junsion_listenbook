<?php
global $_W,$_GPC;
$op = $_GPC['op'] ? $_GPC['op'] : 'display';
$weid = $_W['uniacid'];
if($op == 'display'){
	$condition = '';
	$pindex = max(1,$_GPC['page']);
	$psize = 20;
	
	$nickname = $_GPC['nickname'];
	if(!empty($nickname)){
		$condition .= " and nickname like '%{$nickname}%'";
	}
	$list = getall('select * from '.tb('mem')." where uniacid='{$weid}' {$condition} order by id desc limit ".($pindex-1)*$psize.','.$psize);
	foreach ($list as $k => $v){
		$list[$k]['avatar'] = str_replace('132132', '132', $v['avatar']);
	}
	$total = col('select count(1) from ' . tb('mem') . " where uniacid='{$weid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}

if($op == 'del'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('mem') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('mem'),'error');
	}
	if(del('mem',array('id'=>$id)) === false) MSG('操作失败',referer(),'error');
	else MSG('操作成功',$this->wurl('mem'),'success');
}
if($op == 'score'){
	$id = $_GPC['id'];
	$pindex = max(intval($_GPC['page']), 1);
	$psize = 20;
	$condition = "";
	$start = $_GPC['dateline']?strtotime($_GPC['dateline']['start']):(time()-30*24*3600);
	$end = $_GPC['dateline']?strtotime($_GPC['dateline']['end']):time();
	if(!empty($_GPC['dateline'])) $condition .= " and createtime >= '{$start}' and  createtime <= '{$end}'";
	$limit = "limit " . (($pindex -1) * $psize).','. $psize;
	$list = getall('select * from ' . tb('score_log') . " where mid = '{$id}' {$condition} order by createtime desc {$limit}");
	$total = col('select count(1) from ' . tb('score_log') . " where mid = '{$id}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}

if($op == 'credit'){
	$mid = $_GPC['id'];
	$pindex = max(intval($_GPC['page']), 1);
	$psize = 20;
	$condition = "";
	$start = $_GPC['dateline']?strtotime($_GPC['dateline']['start']):(time()-30*24*3600);
	$end = $_GPC['dateline']?strtotime($_GPC['dateline']['end']):time();
	if(!empty($_GPC['dateline'])) $condition .= " and createtime >= '{$start}' and  createtime <= '{$end}'";
	$limit = "limit " . (($pindex -1) * $psize).','. $psize;
	$list = getall('select * from ' . tb('mcredit') . " where mid = '{$mid}' {$condition} order by createtime desc {$limit}");
	$total = col('select count(1) from ' . tb('mcredit') . " where mid = '{$mid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}

if($op == 'next'){
	$mid = $_GPC['id'];
	$pindex = max(intval($_GPC['page']), 1);
	$psize = 20;
	$condition = "";
	if(!empty($_GPC['keyword'])){
		$condition .= " and nickname like '%{$_GPC['keyword']}%' ";
	}
	$start = $_GPC['dateline']?strtotime($_GPC['dateline']['start']):(time()-30*24*3600);
	$end = $_GPC['dateline']?strtotime($_GPC['dateline']['end']):time();
	if(!empty($_GPC['dateline'])){
		$condition .= " and createtime >= '{$start}' and  createtime <= '{$end}'";
	}
	$levelCommissionCon = '';
	$level = intval($_GPC['level']);
	if($level==2){
		$levelAgentids2 = $this->getLevelAgent($mid,1);
		if(!empty($levelAgentids2)){
			$levelAgentids3 = array();
			$tmpLevelAgentIds = array();
			foreach ($levelAgentids2 as $kl => $vl){
				$level3ids[] = $vl;
			}
			$tmpLevelAgentIds = getall('select id from ' .tb('mem'). " where uniacid='{$weid}' and agentid in (".implode(',', $level3ids).")");
			foreach ($tmpLevelAgentIds as $k => $v){
				if(!empty($v))$levelAgentids3[] = $v['id'];
			}
			if(!empty($levelAgentids3)) $condition .= " and id in (".implode(',', $levelAgentids3).") ";
			else $condition .= " and id = 0 ";
		}
		else{
			$condition .= " and id = 0 ";
		}
	}
	elseif($level==1){
		$levelAgentids2 = $this->getLevelAgent($mid,1);
		if(!empty($levelAgentids2)){
			$condition .= " and id in (".implode(',', $levelAgentids2).") ";
		}
		else{
			$condition .= " and id = 0 ";
		}
	}
	else{
		$condition .= " and agentid = '{$mid}' ";
	}
	$limit = "limit " . (($pindex -1) * $psize).','. $psize;
	$list = getall('select * from ' . tb('mem') . " where uniacid='{$weid}' {$condition} and (avatar != '' or nickname != '') order by createtime desc {$limit}");
	if(!empty($list)){
		foreach ($list as $k => $v){
			$mids[] = $v['id'];
		}
	}
	$total = col('select count(1) from ' . tb('mem') . " where uniacid='{$weid}' {$condition} and (avatar != '' or nickname != '')");
	$firstAgent = getall('select id from ' . tb('mem') . " where agentid = '{$mid}' and (avatar != '' or nickname != '')");
	$first = empty($firstAgent)?0:count($firstAgent);
	if(!empty($firstAgent)){
		foreach ($firstAgent as $k => $v){
			$f_mids[] = $v['id'];
		}
	}
	$levelAgentids2Total = $this->getLevelAgent($mid,1);
	$second = empty($levelAgentids2Total)?0:count($levelAgentids2Total);
	if(!empty($levelAgentids2Total)){
		foreach ($levelAgentids2Total as $k => $v){
			$s_mids[] = $v;
		}
		$levelAgentids3Total = array();
		$tmpLevelAgentIdsTotal = array();
		foreach ($levelAgentids2Total as $kl => $vl){
			$level3idsTotal[] = $vl;
		}
		$tmpLevelAgentIdsTotal = getall('select id from ' .tb('mem'). " where uniacid='{$weid}' and agentid in (".implode(',', $level3idsTotal).")");
		foreach ($tmpLevelAgentIdsTotal as $k => $v){
			if(!empty($v))$levelAgentids3Total[] = $v['id'];
		}
		if(!empty($levelAgentids3Total)) {
			foreach ($levelAgentids3Total as $k => $v){
				$t_mids[] = $v;
			}
			$condition1 .= " and id in (".implode(',', $levelAgentids3Total).") ";
		}
		else $condition1 .= " and id = 0 ";
		$third = col('select count(1) from ' . tb('mem') . " where uniacid='{$weid}' {$condition1} and (avatar != '' or nickname != '')");
	}
	$pager = pagination($total, $pindex, $psize);
}
include $this->template('mem');