<?php
global $_W,$_GPC;
$op = $_GPC['op'] ? $_GPC['op'] : 'display';
$weid = $_W['uniacid'];
if($op == 'display'){
	$condition = '';
	$pindex = max(1,$_GPC['page']);
	$psize = 20;
	if($_GPC['type'] != ''){
		$condition .= " and type = '{$_GPC['type']}'";
	}
	if($_GPC['status'] != ''){
		$condition .= " and status = '{$_GPC['status']}'";
	}
	$list = getall('select * from '.tb('poster')." where uniacid='{$weid}' {$condition} order by id desc limit ".($pindex-1)*$psize.','.$psize);
	$total = col('select count(1) from ' . tb('poster') . " where uniacid='{$weid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}

if($op == 'post'){
	$id = intval($_GPC['id']);
	if ($id){
		$item = get('select * from '.tb('poster')." where id='{$id}' and uniacid='{$weid}'");
		$poster = unserialize($item['poster']);
	}
	
	if (checksubmit()){
		$_GPC['poster']['data'] = json_decode(htmlspecialchars_decode($_GPC['poster']['data']),true);
		$data = array(
			'status' => trim($_GPC['status']),
			'bid' => trim($_GPC['bid']),
			'poster' => serialize($_GPC['poster'])
		);
		if($data['status'] == 1) update('poster',['status'=>0],['type'=>$data['type']]);
		if ($item){
			update('poster',$data,array('id'=>$id));
		}else{
			$data['type'] = trim($_GPC['type']);
			$data['uniacid'] = $weid;
			insert('poster', $data);
		}
		MSG('操作成功',$this->wurl('poster',array('op'=>'display')),'success');
	}
	
	$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}'");
}
		
if($op == 'del'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('poster') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('poster'),'error');
	}
	if(del('poster',array('id'=>$id)) === false) MSG('操作失败',referer(),'error');
	else MSG('操作成功',$this->wurl('poster'),'success');
}
include $this->template('poster_');
