<?php
global $_GPC, $_W;
$mid = $_GPC['mid'];
$pagesize = 15;
$pageindex = max(1, intval($_GPC['page']));
$limit = "limit " . (($pageindex -1) * $pagesize).','. $pagesize;
$condition = '';
if($_GPC['type']==1){
	$levelAgentids2 = $this->getLevelAgent($mid,1);
	if(!empty($levelAgentids2)){
		$condition .= " and id in (".implode(',', $levelAgentids2).") ";
	}
	else{
		$condition .= " and id = 0 ";
	}
}
elseif($_GPC['type']==2){
	$levelAgentids2 = $this->getLevelAgent($mid,1);
	if(!empty($levelAgentids2)){
		$levelAgentids3 = array();
		$tmpLevelAgentIds = array();
		foreach ($levelAgentids2 as $kl => $vl){
			$level3ids[] = $vl;
		}
		$tmpLevelAgentIds = getall('select id from ' .tb('mem'). " where agentid in (".implode(',', $level3ids).")");
		foreach ($tmpLevelAgentIds as $k => $v){
			if(!empty($v))$levelAgentids3[] = $v['id'];
		}
		if(!empty($levelAgentids3)) $condition .= " and id in (".implode(',', $levelAgentids3).") ";
		else $condition .= " and id = 0 ";
	}
	else{
		$condition .= " and id = 0 ";
	}
}
else{
	$condition .= " and agentid = '{$mid}' ";
}
$list = getall('select id,nickname,avatar,createtime from ' . tb('mem') . " where uniacid='{$_W['uniacid']}' and (avatar != '' or nickname != '') {$condition} order by createtime desc {$limit}");
if(!empty($list)){
	foreach ($list as $k => $v){
		$mids[] = $v['id'];
	}
	foreach ($list as $k => $v){
		$list[$k]['createtime'] = date('Y-m-d H:i',$v['createtime']);
		$list[$k]['nickname'] = substr_cut($list[$k]['nickname']);
		$list[$k]['haspay'] = col("select count(1) from ".tb("order")." where mid='{$v['id']}' and status=1")>0;
	}
}
if($_GPC['scroll']){
	if(!empty($list)){
		$data = array(
				'status' => '1',
				'lists' => $list,
		);
	}else{
		$data = array('status'=>'0','lists' => $list);
	}
	die(json_encode($data));
}