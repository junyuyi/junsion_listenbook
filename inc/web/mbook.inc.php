<?php
global $_GPC,$_W;
$weid = $_W['uniacid'];
$op = empty($_GPC['op'])?'display':$_GPC['op'];
if($op == 'display'){
	$psize = 20;
	$pindex = max(1,$_GPC['page']);
	
	$condition = " uniacid='{$weid}'";
	$nickname = $_GPC['nickname'];
	$btitle = $_GPC['btitle'];
	if (!empty($nickname)){
		$mems = getall('select id,avatar,nickname from '.tb('mem')." where uniacid='{$weid}' and nickname like '%{$nickname}%'",'id');
		if (!empty($mems)){
			$mids = array_keys($mems);
			$condition .= " and mid in (".implode(',', $mids).")";
		}else{
			$condition .= " and mid = 0";
		}
	}
	if (!empty($btitle)){
		$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}' and title like '%{$btitle}%'",'id');
		if (!empty($books)){
			$bids = array_keys($books);
			$condition .= " and bid in (".implode(',', $bids).")";
		}else{
			$condition .= " and bid = 0";
		}
	}
	$list = getall('select * from '.tb('mem_book')." where {$condition} order by createtime desc limit ".($pindex-1)*$psize.",".$psize);
	$total = col('select * from '.tb('mem_book')." where {$condition}");
	$page = pagination($total,$pindex,$pindex);
	
	if (!empty($list)){
		foreach ($list as $l){
			$mids[] = $l['mid'];
			$bids[] = $l['bid'];
		}
		if (empty($nickname)){
			$mems = getall('select id,avatar,nickname from '.tb('mem')." where uniacid='{$weid}' and id in (".implode(',', $mids).")",'id');
		}
		if (empty($btitle)){
			$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}' and id in (".implode(',', $bids).")",'id');
		}
	}
	
}
elseif($op=='test'){
	$psize = 20;
	$pindex = max(1,$_GPC['page']);
	$id = $_GPC['id'];
	
	$condition = " uniacid='{$weid}' and bmid = '{$id}'";
	$nickname = $_GPC['nickname'];
	$btitle = $_GPC['btitle'];
	$dtitle = $_GPC['dtitle'];
	if (!empty($nickname)){
		$mems = getall('select id,avatar,nickname from '.tb('mem')." where uniacid='{$weid}' and nickname like '%{$nickname}%'",'id');
		if (!empty($mems)){
			$mids = array_keys($mems);
			$condition .= " and mid in (".implode(',', $mids).")";
		}else{
			$condition .= " and mid = 0";
		}
	}
	if (!empty($btitle)){
		$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}' and title like '%{$btitle}%'",'id');
		if (!empty($books)){
			$bids = array_keys($books);
			$condition .= " and bid in (".implode(',', $bids).")";
		}else{
			$condition .= " and bid = 0";
		}
	}
	if (!empty($dtitle)){
		$directors = getall('select id,title from '.tb('book_directory')." where uniacid='{$weid}' and title like '%{$dtitle}%'",'id');
		if (!empty($directors)){
			$dids = array_keys($directors);
			$condition .= " and did in (".implode(',', $dids).")";
		}else{
			$condition .= " and bid = 0";
		}
	}
	$list = getall('select * from '.tb('mem_exam')." where {$condition} order by createtime desc limit ".($pindex-1)*$psize.",".$psize);
	$total = col('select * from '.tb('mem_exam')." where {$condition}");
	$page = pagination($total,$pindex,$pindex);
	if (!empty($list)){
		foreach ($list as $l){
			$mids[] = $l['mid'];
			$bids[] = $l['bid'];
			$dids[] = $l['did'];
		}
		if (empty($nickname)){
			$mems = getall('select id,avatar,nickname from '.tb('mem')." where uniacid='{$weid}' and id in (".implode(',', $mids).")",'id');
		}
		if (empty($btitle)){
			$books = getall('select id,title from '.tb('book')." where uniacid='{$weid}' and id in (".implode(',', $bids).")",'id');
		}
		if (empty($dtitle)){
			$directors = getall('select id,title from '.tb('book_directory')." where uniacid='{$weid}' and id in (".implode(',', $dids).")",'id');
		}
	}
}
elseif($op=='detail'){
	$id = $_GPC['id'];
	$examid = $_GPC['examid'];
	$item = get('select * from ' .tb('mem_exam'). " where id = '{$examid}'");
	$item['detail'] = unserialize($item['detail']);
	if(!empty($item['detail'])){
		foreach ($item['detail'] as $k => $v){
			$qids[] = $k;
		}
		$questions = getall('select id,title,detail from ' .tb('book_question'). " where id in (".implode(',', $qids).")",'id');
		foreach ($questions as $k => $v){
			$questions[$k]['detail'] = unserialize($v['detail']);
		}
	}
	$book = get('select id,title from ' .tb('book'). " where id = '{$item['bid']}'");
	$directory = get('select id,title from ' .tb('book_directory'). " where id = '{$item['did']}'");
}
include $this->template('mbook');