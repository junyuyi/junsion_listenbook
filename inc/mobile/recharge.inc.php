<?php
global $_W,$_GPC;
$title = '成长星充值';
$mem = $this->getMem();
$cfg = $this->module['config'];
if ($cfg['creditswitch']==1){
	load()->model('mc');
	$fans = mc_fetch($mem['openid'],array('credit2','uid'));
}
if($_W['isajax']){
	if(empty($_GPC['money']) || $_GPC['money']<0){
		resp(['code'=>0, 'msg'=>'充值金额错误']);
	}
	$_GPC['money'] = intval($_GPC['money'] * 100) / 100;
	$score = $_GPC['money'];
	foreach ($cfg['recharge'] as $k => $v){
		if($v['money'] == $_GPC['money']){
			$score = $v['score'];
			break;
		}
	}
	$ordersn = 'J'.date('YmdHis').random(6);
	$paytype = $_GPC['paytype'];
	$data = [
			'uniacid' => $_W['uniacid'],
			'ordersn' => $ordersn,
			'paytype' => $paytype,
			'price' => $_GPC['money'],
			'score' => intval($score),
			'mid' => $mem['id'],
			'createtime' => TIMESTAMP
	];
	if ($paytype==2 && $date['price']>$fans['credit2']){
		resp(['code'=>0, 'msg'=>'余额不足，剩余余额：'.$fans['credit2']]);
	}
	insert('recharge', $data);
	$newid = pdo_insertid();
	if($newid>0){
		if ($paytype==1){
			$preresult = $this->unifiedOrder('充值成长星',$_GPC['money'], $ordersn, 'recharge');
			$result = array();
			if($preresult['errcode'] == 0){
				$params = $this->getWxPayJsParams($preresult['prepay_id']);
				$result = array(
						"appId"=>$_W['account']['key'],
						"timeStamp"=>$params['timeStamp'],
						"nonceStr"=>$params['nonceStr'],
						"package"=>$params['package'],
						"signType"=>$params['signType'],
						"paySign"=>$params['paySign'],
						"orderid"=>$newid,
				);
				$arr = array(
						'code' => 1,
						'res' => $result
				);
				resp($arr);
			}else{
				resp(['code'=>0, 'msg'=>$preresult['errmsg']]);
			}
		}elseif ($paytype==2){
			mc_credit_update($fans['uid'],'credit2',-$data['price']);
			$ret = array();
			$ret['weid'] = $_W['weid'];
			$ret['uniacid'] = $_W['uniacid'];
			$ret['result'] = 'success';
			$ret['type'] = 'credit';
			$ret['table'] = 'recharge';
			$ret['tid'] = $ordersn;
			$ret['from'] = 'notify';
			$ret['user'] = $mem['openid'];
			$ret['fee'] = $data['price'];
			$this->payResult($ret);
			resp(array('code'=>2,'orderid'=>$newid));
		}
	}
	else{
		resp(['code'=>0, 'msg'=>'更新记录错误']);
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('recharge');