<?php

if (!pdo_fieldexists('junsion_listenbook_book_chapter', 'type')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_book_chapter') . " ADD `type` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '类型 0音频 1视频';");
}
if (!pdo_fieldexists('junsion_listenbook_book', 'poster')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_book') . " ADD `poster` text;");
}
if (!pdo_fieldexists('junsion_listenbook_book', 'commission')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_book') . " ADD `commission` text;");
}
if (!pdo_fieldexists('junsion_listenbook_book', 'booktips')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_book') . " ADD `booktips` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '精选书单';");
}
if (!pdo_fieldexists('junsion_listenbook_poster', 'bid')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_poster') . " ADD `bid` INT(11) NOT NULL DEFAULT '0';");
}
if (!pdo_fieldexists('junsion_listenbook_order', 'paytype')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_order') . " ADD `paytype` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '支付方式 1微信 2余额';");
}
if (!pdo_fieldexists('junsion_listenbook_recharge', 'paytype')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_recharge') . " ADD `paytype` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '支付方式 1微信 2余额';");
}
if (!pdo_fieldexists('junsion_listenbook_bremark', 'cid')) {
	pdo_query("ALTER TABLE " . tablename('junsion_listenbook_bremark') . " ADD `cid` INT(11) NOT NULL DEFAULT '0'");
}