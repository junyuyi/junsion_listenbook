<?php
global $_GPC, $_W;
$cfg = $this->module['config'];
$mem = $this->getMem();
$withMid = $mem['id'];
if(!empty($cfg['commission']['withnum'])){
	$withNums = col('select count(1) from ' . tb('with') . " where mid = '{$withMid}' and uniacid ='{$_W['uniacid']}' and to_days(from_unixtime(createtime)) = to_days(now())");
	if($withNums>=$cfg['commission']['withnum']){
		$arr = array(
				'status' => 0,
				'msg' => '今日提现次数已达上限，请于明天再来提现'
		);
		resp($arr);
	}
}
$money = $mem['credit'];
if($money>20000 && $cfg['commission']['wtype']==0) $money = 20000;
if($money<$cfg['commission']['with'] || $money>$mem['credit']){
	$arr = array(
			'status' => 0,
			'msg' => '提现金额错误'
	);
	resp($arr);
}
$wrate = 0;
if($cfg['commission']['wrate']>0){
	$wrate = floor(floatval($money * $cfg['commission']['wrate'])) / 100;
}
$reduce = $money;
$money = $money - $wrate;
$data = array(
		'uniacid' => $_W['uniacid'],
		'price' => $money,
		'wrate' => $wrate,
		'mid' => $mem['id'],
		'createtime' => time(),
);
insert('with',$data);
$newid = pdo_insertid();
if($newid>0){
	if(!empty($_GPC['qrPic'])) update('mem', array('qr'=>$_GPC['qrPic']), array('id'=>$mem['id']));
	//用户余额日志
	updateMemCredit(array('mid'=>$mem['id'], 'credit'=> -$reduce, 'remark'=>'提现'));
	//是否自动打款
	if($cfg['commission']['wtype']==0 && (($money<=$cfg['commission']['check'] && !empty($cfg['commission']['check'])) || empty($cfg['commission']['check']))){
		$params = array('price'=>$money);
		$params['openid'] = $mem['openid'];
		$res = $this->sendRedPacket($params);
		if($res['code']==1){
			update('with',array('status'=>1,'transid'=>$res['msg']),array('id'=>$newid));
			$arr = array(
					'status' => 1,
					'msg' => '提现成功，请注意查收信息'
			);
		}
		else{
			$arr = array(
					'status' => 0,
					'msg' => $res['msg']
			);
		}
		resp($arr);
	}
	
	$arr = array(
			'status' => 1,
			'msg' => '申请提现成功，请耐心等候审核'
	);
	resp($arr);
}
else{
	$arr = array(
			'status' => 0,
			'msg' => '更新提现记录失败，请稍后重试'
	);
	resp($arr);
}
