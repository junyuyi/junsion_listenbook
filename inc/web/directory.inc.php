<?php
global $_W,$_GPC;
$bid = $_GPC['bid'];
$op = $_GPC['op'] ? $_GPC['op'] : 'display';
$weid = $_W['uniacid'];

$book = get('select * from '.tb('book')." where id='{$bid}' and uniacid='{$weid}'");
if (empty($book)) MSG('课堂不存在',referer(),'error');
if($op == 'display'){
	$condition = '';
	$pindex = max(1,$_GPC['page']);
	$psize = 20;
	
	$title = $_GPC['title'];
	if(!empty($title)){
		$condition .= " and title like '%{$title}%'";
	}
	$list = getall('select * from '.tb('book_directory')." where uniacid='{$weid}' and bid='{$bid}' {$condition} order by sort desc, id asc limit ".($pindex-1)*$psize.','.$psize);
	$total = col('select count(1) from ' . tb('book_directory') . " where uniacid='{$weid}' and bid='{$bid}' {$condition}");
	$pager = pagination($total, $pindex, $psize);
}
		
if($op == 'post'){
	$id = intval($_GPC['id']);
	if ($id){
		$item = get('select * from '.tb('book_directory')." where id='{$id}'");
	}
	
	if (checksubmit()){
		$data = array(
			'title' => trim($_GPC['title']),
			'sort' => trim($_GPC['sort']),
		);
		if ($item){
			update('book_directory',$data,array('id'=>$id));
		}else{
			$data['bid'] = $bid;
			$data['uniacid'] = $weid;
			insert('book_directory', $data);
		}
		MSG('操作成功',$this->wurl('directory',array('op'=>'display','bid'=>$bid)),'success');
	}
}
		
if($op == 'del'){
	$id = $_GPC['id'];
	$item = get('select * from ' . tb('book_directory') . " where id = '{$id}'");
	if(empty($item)){
		MSG('该记录不存在或已被删除',$this->wurl('directory',array('bid'=>$bid)),'error');
	}
	if(del('book_directory',array('id'=>$id)) === false) MSG('操作失败',referer(),'error');
	else{
		del('book_chapter',array('did'=>$id));
		MSG('操作成功',$this->wurl('directory',array('bid'=>$bid)),'success');
	}
}
include $this->template('directory');