<?php
global $_W,$_GPC;
$title = '学生证';
$mem = $this->getMem();
$mem['avatar'] = $this->saveImage($mem['avatar'],'avatar_'.$mem['id']);
$mem['avatar'] = str_replace(IA_ROOT.'/', $_W['siteroot'], $mem['avatar'].'?v='.TIMESTAMP);
if($_W['isajax']){
	$op = empty($_GPC['op'])?'display':$_GPC['op'];
	switch($op){
		case 'display':
			$mem_book = get('select id,bid,studentid,starttime,guide_weid from ' .tb('mem_book'). " where id = '{$_GPC['bid']}' and mid = '{$mem['id']}' and status = 0");
			if(empty($mem_book)) resp(['code'=>0,'msg'=>'未购买该课程']);
			$book = get('select id,title,class_num from ' .tb('book'). " where id = '{$mem_book['bid']}'");
			if(empty($book)) resp(['code'=>0,'msg'=>'课程不存在']);
			$poster = get('select id,poster from ' .tb('poster'). " where type = 1 and status = 1");
			if(empty($poster)) resp(['code'=>0,'msg'=>'系统错误，请联系管理员']);
			$poster['poster'] = unserialize($poster['poster']);
			$poster['poster']['bg'] = getImgBase64(toimage($poster['poster']['bg']));
			$poster['btitle'] = $book['title'];
			$poster['class_num'] = $book['class_num'];
			$poster['studentid'] = $mem_book['studentid'];
			$time = strtotime('+ '.$book['class_num'].'days', $mem_book['starttime']);
			$poster['time'] = date('Y-m-d', $time);
			unset($book);
			unset($mem_book);
			if(empty($mem_book['guide_weid'])) $mem_book['guide_weid'] = $_W['uniacid'];
			$qrUrl = $this->getQR(array('wid'=>$mem_book['guide_weid'],'mid'=>$mem['id']));
			$poster['qurl'] = imgtobase64($qrUrl);
			resp(['code'=>1,'poster'=>$poster]);
			break;
	}
}
else{
	$share = $this->getShare($mem, $cfg);
}
$cfg = $this->module['config'];
$img = toimage($cfg['studentID_cover']);
include $this->template('studentID');